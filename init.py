import os

# Generate the random string which will be used to encode / decode data on the server
os.system('dd if=/dev/urandom count=1 bs=28 | xxd -p > /ramdisk/key.clear')

# Generate the random string used to encode the server's key
os.system('dd if=/dev/urandom count=1 bs=28 | xxd -p > /ramdisk/private.tmp')
key2 = ""
with open('/ramdisk/private.tmp') as f:
    key2 = f.read()

# Encrypt and store the key on HDD.
os.system(f'openssl enc -a -K $(cat /ramdisk/private.tmp) -iv 0 -aes256 -in /ramdisk/key.clear -out ./private.key')

#Creating the first usb file for responsable
firsthalf = key2[0:len(key2)//2] + "res"
with open('/ramdisk/.firsthalf', 'w') as f:
    f.write(firsthalf)
os.system('openssl enc -a -aes256 -pbkdf2 -in /ramdisk/.firsthalf -out usb/firstEnc.key')
os.remove('/ramdisk/.firsthalf')

#Creating the first usb file for representant
firsthalf = key2[0:len(key2)//2] + "rep"
with open('/ramdisk/.firsthalf', 'w') as f:
    f.write(firsthalf)
os.system('openssl enc -a -aes256 -pbkdf2 -in /ramdisk/.firsthalf -out usb3/firstEnc.key')
os.remove('/ramdisk/.firsthalf')

#Create the second usb file
secondhalf = key2[len(key2)//2:] + "res"
with open('/ramdisk/.secondhalf', 'w') as f:
    f.write(secondhalf)
os.system('openssl enc -a -aes256 -pbkdf2 -in /ramdisk/.secondhalf -out usb2/secondEnc.key')
os.remove('/ramdisk/.secondhalf')

#Create the second usb file
secondhalf = key2[len(key2)//2:] + "rep"
with open('/ramdisk/.secondhalf', 'w') as f:
    f.write(secondhalf)
os.system('openssl enc -a -aes256 -pbkdf2 -in /ramdisk/.secondhalf -out usb4/secondEnc.key')
os.remove('/ramdisk/.secondhalf')

os.remove("/ramdisk/key.clear")
os.remove("/ramdisk/private.tmp")