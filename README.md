# Samier Vincent ISI TP3

## Questions

### Q1

+ Pour ce que j'ai, on peut demander un fichier particulier à chacun des responsables. C'est un fichier unique contenant une partie de la clé de chiffrement.
+ Pour ce que je sais, chaque responsable doit avoir entré un mot de passe impossible à trouver en un temps raisonnable lors de la mise en service du serveur.

### Q3

On peut ajouter un élément qui permette de différencier un responsable d'un représentant dans la clé qu'ils possèdent. Par exemple, on peut ajouter rep avant de crypter la clé d'un représentant, et res pour celle d'un responsable. Il suffira de regarder les trois caractères à la fin de la clé pour savoir qui a mit en service le serveur.

### Q5

Pour rendre impossible la répudiation sans l'ensemble des possesseurs de code présents, il faut que le serveur attende 3 porteurs de code. Il faut ensuite tester les deux combinaisons possibles obtenues en ayant 3 membres rassemblés. Si les deux combinaisons peuvent décrypter la clé, alors on peut répudier le dernier membre. On modifie donc la clé encrypté sur le HDD, et on met à jour tous les codes des possesseurs.