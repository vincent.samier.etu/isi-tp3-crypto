#!/usr/bin/env python3

import mysql.connector
import cherrypy
import config
import html
import os

class SecureServer(object):
    def __init__(self):
        self.firstUser = ""
        self.secondUser = ""
        self.setupKey()
        self.conn = mysql.connector.connect(host=config.DB_HOST, user=config.DB_USER, database=config.DB_NAME, password=config.DB_PASS)

    def setupKey(self):
        os.system('openssl enc -a -aes256 -pbkdf2 -d -in usb/firstEnc.key -out /ramdisk/first.clear')
        os.system('openssl enc -a -aes256 -pbkdf2 -d -in usb2/secondEnc.key -out /ramdisk/second.clear')
        first = ""
        with open('/ramdisk/first.clear') as f:
            first = f.read()
        os.remove('/ramdisk/first.clear')
        second = ""
        with open('/ramdisk/second.clear') as f:
            second = f.read()
        os.remove('/ramdisk/second.clear')
        self.firstUser = first[len(first) - 3:]
        self.secondUser = second[len(second) - 3:]
        key = first[:len(first) - 3] + second[:len(second) -3]
        with open('/ramdisk/decrypt.clear', 'w') as f:
            f.write(key)
        os.system(f'openssl enc -a -d -K $(cat /ramdisk/decrypt.clear) -iv 0 -aes256 -in ./private.key -out /ramdisk/key.clear')


    @cherrypy.expose
    def index(self, **post):
        cursor = self.conn.cursor(prepared=True)
        if cherrypy.request.method == "POST":
            requete = """ INSERT INTO cartebancaire (name,cardnumber) VALUES (%s,%s) """
            with open('/ramdisk/card', 'w') as f:
                f.write(html.escape(post["carte"]))
            os.system('openssl enc -a -aes256 -K $(cat /ramdisk/key.clear) -iv 0  -pbkdf2 -in /ramdisk/card -out /ramdisk/card.crypt')
            cardCrypt = ""
            with open('/ramdisk/card.crypt') as f:
                cardCrypt = f.read()
            with open('/ramdisk/name', 'w') as f:
                f.write(html.escape(post["carte"]))
            os.system('openssl enc -a -aes256 -K $(cat /ramdisk/key.clear) -iv 0  -pbkdf2 -in /ramdisk/name -out /ramdisk/name.crypt')
            nameCrypt = ""
            with open('/ramdisk/name.crypt') as f:
                nameCrypt = f.read()
            data = [nameCrypt, cardCrypt]
            cursor.execute(requete, data)
            self.conn.commit()


        if cherrypy.request.method == "DELETE":
            requete = """DELETE FROM cartebancaire WHERE cardnumber = (%s)"""
            with open('/ramdisk/card', 'w') as f:
                f.write(html.escape(post["carte"]))
            os.system('openssl enc -a -aes256 -K $(cat ramdisk/key.clear) -iv 0  -pbkdf2 -in /ramdisk/card -out /ramdisk/card.crypt')
            cardCrypt = ""
            with open('/ramdisk/card.crypt') as f:
                cardCrypt = f.read()
            data = [cardCrypt]
            cursor.execute(requete, data)
            self.conn.commit()

        chaines = []
        cursor.execute("SELECT name,cardnumber FROM cartebancaire");
        for row in cursor.fetchall():
            chaines.append(row[0] + " envoye par: " + row[1])

        cursor.close()
        return '''
<html>
<head>
<title>Application Python Vulnerable</title>
</head>
<body>
<p>
Bonjour, je suis une application vulnerable qui sert a inserer des chaines dans une base de données MySQL!
</p>

<p>
Liste des chaines actuellement insérées:
<ul>
'''+"\n".join(["<li>" + s + "</li>" for s in chaines])+'''
</ul>
</p>

<p> Inserer une chaine:

Insérer une entrée
<form method="post" onsubmit="return validate()">
<input type="text" name="carte" id="carte" value="" />
<br />
<input type="submit" name="submit" value="OK" />
</form>

Supprimer une entrée
<form method="post" onsubmit="">
<input type="text" name="carte" id="carte" value="" />
<br />
<input type="submit" name="submit" value="OK" />
</form>

<script>
function validate() {
    var regex = /^[a-zA-Z0-9]+$/;
    var chaine = document.getElementById('chaine').value;
    console.log(regex.test(chaine));
    if (!regex.test(chaine)) {
        alert("Veuillez entrer une chaine avec uniquement des lettres et des chiffres");
        return false;
    }
    return true;
}
</script>

</p>
</body>
</html>
'''


cherrypy.quickstart(SecureServer())
