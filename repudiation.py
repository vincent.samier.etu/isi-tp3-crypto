import os

print("Selectionnez la personne à répudier")
print("1 : Le titulaire de la première partie")
print("2 : Le représentant de la première partie")
print("3 : Le titulaire de la deuxième partie")
print("4 : Le représentant de la première partie")
n = input()
if (n != "1") :
    os.system('openssl enc -a -aes256 -pbkdf2 -d -in usb/firstEnc.key -out /ramdisk/firstTit.clear')
if (n != "2") :
    os.system('openssl enc -a -aes256 -pbkdf2 -d -in usb3/firstEnc.key -out /ramdisk/firstRes.clear')
if (n != "3") :
    os.system('openssl enc -a -aes256 -pbkdf2 -d -in usb2/secondEnc.key -out /ramdisk/secondTit.clear')
if (n != "4") :
    os.system('openssl enc -a -aes256 -pbkdf2 -d -in usb4/firstEnc.key -out /ramdisk/secondRes.clear')
if (n == "1" or n == "2"):
    firstRes = ""
    secondTit = ""
    secondRes = ""
    if (n == "1"):
        with open('/ramdisk/firstRes.clear') as f:
            firstRes = f.read()
            firstRes = firstRes[:-3]
    if (n == "2"):
        with open('/ramdisk/firstTit.clear') as f:
            firstRes = f.read()
            firstRes = firstRes[:-3]
    with open('/ramdisk/secondTit.clear') as f:
        secondTit = f.read()
        secondTit = secondTit[:-3]
    with open('/ramdisk/firstRes.clear') as f:
        secondRes = f.read()
        secondRes = secondRes[:-3]
    with open('/ramdisk/decrypt1.clear', 'w') as f:
        f.write(firstRes + secondTit)
    with open('/ramdisk/decrypt2.clear', 'w') as f:
        f.write(firstRes + secondRes)
if (n == "3" or n == "4"):
    secondRes = ""
    firstTit = ""
    firstRes = ""
    if (n == "3"):
        with open('/ramdisk/secondRes.clear') as f:
            secondRes = f.read()
            secondRes = secondRes[:-3]
    if (n == "4"):
        with open('/ramdisk/secondTit.clear') as f:
            secondRes = f.read()
            secondRes = secondRes[:-3]
    with open('/ramdisk/secondTit.clear') as f:
        firstTit = f.read()
        firstTit = firstTit[:-3]
    with open('/ramdisk/firstRes.clear') as f:
        firstRes = f.read()
        firstRes = firstRes[:-3]
    with open('/ramdisk/decrypt1.clear', 'w') as f:
        f.write(firstRes + secondRes)
    with open('/ramdisk/decrypt2.clear', 'w') as f:
        f.write(firstTit + secondRes)
if os.system('openssl enc -a -d -K $(cat /ramdisk/decrypt1.clear) -iv 0 -aes256 -in ./private.key -out /ramdisk/keytest.clear') != 0:
    print("Error revoking rights")
    exit()
if os.system('openssl enc -a -d -K $(cat /ramdisk/decrypt2.clear) -iv 0 -aes256 -in ./private.key -out /ramdisk/keytest.clear') != 0:
    print("Error revoking rights")
    exit()
    
# Generate the random string used to encode the server's key
os.system('dd if=/dev/urandom count=1 bs=28 | xxd -p > /ramdisk/private.tmp')
key2 = ""
with open('/ramdisk/private.tmp') as f:
    key2 = f.read()
# Encrypt and store the key on HDD.
os.system(f'openssl enc -a -K $(cat /ramdisk/private.tmp) -iv 0 -aes256 -in /ramdisk/key.clear -out ./private.key')
#Creating the first usb file for responsable
firsthalf = key2[0:len(key2)//2] + "res"
with open('/ramdisk/.firsthalf', 'w') as f:
    f.write(firsthalf)
os.system('openssl enc -a -aes256 -pbkdf2 -in /ramdisk/.firsthalf -out usb/firstEnc.key')
os.remove('/ramdisk/.firsthalf')
#Creating the first usb file for representant
firsthalf = key2[0:len(key2)//2] + "rep"
with open('/ramdisk/.firsthalf', 'w') as f:
    f.write(firsthalf)
os.system('openssl enc -a -aes256 -pbkdf2 -in /ramdisk/.firsthalf -out usb3/firstEnc.key')
os.remove('/ramdisk/.firsthalf')
#Create the second usb file
secondhalf = key2[len(key2)//2:] + "res"
with open('/ramdisk/.secondhalf', 'w') as f:
    f.write(secondhalf)
os.system('openssl enc -a -aes256 -pbkdf2 -in /ramdisk/.secondhalf -out usb2/secondEnc.key')
os.remove('/ramdisk/.secondhalf')
#Create the second usb file
secondhalf = key2[len(key2)//2:] + "rep"
with open('/ramdisk/.secondhalf', 'w') as f:
    f.write(secondhalf)
os.system('openssl enc -a -aes256 -pbkdf2 -in /ramdisk/.secondhalf -out usb4/secondEnc.key')
os.remove('/ramdisk/.secondhalf')
os.remove("/ramdisk/key.clear")
os.remove("/ramdisk/private.tmp")